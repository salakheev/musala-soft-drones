### [Musala](https://www.musala.com/) Test Application

This simple project is designed to disperse drones using REST API. 
All information about their work is stored in a relational database based on [PostgreSQL](https://www.postgresql.org/)

#### Swagger UI url path: 
**http://domain/swagger-ui/#/**

---
According to the [TASK.md](https://gitlab.com/salakheev/musala-soft-drones/-/blob/main/TASK.md) file, 
our drone fleet contains 10 different models of drones (different in carrying capacity), drones data is downloaded when the app is first launched by [migration scripts](https://gitlab.com/salakheev/musala-soft-drones/-/tree/main/src/main/resources/changelog) using Liquibase.
---
#### Used technologies and frameworks:
* Java 
* Spring Boot (Web, Data JPA modules)
* Swagger UI (OpenApi)
* PostgreSQL  
* Liquibase 
* MapStruct
* ShedLock

#### Build & deployment tools:
* Gradle 
* Docker 
---

### Build, package & run:
Be sure you got to have installed Docker. 
1. _To Start_: run the command below in project's root directory using Terminal or CMD
```bash
./gradlew jar & docker-compose up
```
**My congrats, if you see this log messages:**\
![startup.png](./readme/startup.png)

2. _To stop_: run the next command in the same directory:
```bash
docker-compose down
```
---
#### Additional Links
These additional references should also help you:
* [About me](https://github.com/salaheev)

